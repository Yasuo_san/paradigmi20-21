package u02

object Shape extends App{
  sealed trait Shape
  object Shape {

    case class Rectangle(side1: Double, side2: Double) extends Shape

    case class Circle(radius: Double) extends Shape

    case class Square(side: Double) extends Shape

    def perimeter(s: Shape): Double = s match {
      case Rectangle(n, m) => 2*(n + m)
      case Circle(n) => 2*n*math.Pi
      case Square(n) => n*4
    }

    def area(s: Shape): Double = s match {
      case Rectangle(n, m) => n*m
      case Circle(n) => n*n*math.Pi
      case Square(n) => n*n
    }

  }
}
