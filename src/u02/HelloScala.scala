package u02

object HelloScala extends App {

  ///////////////////////////////////////////////////////2A a)

  val a: Int => String = {
    case n if n % 2 == 0 => "even"
    case n if n % 2 != 0 => "odd"
  }

  println(a(2))
  println(a(1))
  println(a(42))
  println(a(0))
  println(a(157))
  println("")

  def parity(x: Int): String = x match {
    case x if x % 2 == 0 => "even"
    case x if x % 2 != 0 => "odd"
  }

  println(parity(2))
  println(parity(1))
  println(parity(42))
  println(parity(0))
  println(parity(157))
  println("")

  /////////////////////////////////////////////////////////2A b)

  val empty: String => Boolean = _==""
  val neg1: (String => Boolean) => String => Boolean =
    f => i => !f(i)

  def neg2(f : String => Boolean): String => Boolean = {
      i => !f(i)
  }

  val notEmpty1 = neg1(empty)
  val notEmpty2 = neg2(empty)
  println(empty("ciao"))
  println(empty(""))
  println(notEmpty1("ciao"))
  println(notEmpty2("ciao"))
  println(notEmpty1(""))
  println(notEmpty2(""))
  println("")


  ////////////////////////////////////////////////////////////2A c)

  //Non so se sia possibile creare una versione funzionante per i predicati generici


  //////////////////////////////////////////////////////////// 2B  4)

  val p2:(Int,Int,Int) => Boolean =
     (x,y,z) => x<=y && y<=z

  def p4(x : Int, y : Int, z : Int) : Boolean = {
    x<=y && y<=z
  }

  val p1:(Int,Int,Int) => Boolean =
    (x,y,z) => x<=y && y<=z

  def p3(x : Int)(y : Int)(z : Int) : Boolean = {
    x<=y && y<=z
  }

  println(p1(1,2,3))
  println(p1(5,7,3))
  println(p3(1)(2)(3))
  println(p3(5)(7)(3))
  println(p4(1,2,3))
  println(p4(5,7,3))
  println("")

  //////////////////////////////////////////////////////////// 2B  5)

  def compose(f: Int => Int, g: Int => Int): Int => Int = {
    i => f(g(i))
  }

  println(compose(_-1,_*2)(5))
  println(compose(3+_,_/2)(9))
  println("")

  //////////////////////////////////////////////////////////// 3  6)

  def fib(n: Int): Int = n match {
    case 0 => 0
    case 1 | 2 => 1
    case _ => fib(n-1) + fib(n-2)  //// Si tratta di recursive tail perche' la chiamata di ricorsione e' l'ultima
                                   //// ad essere chiamata prima della fine della funzione.
  }

  println(fib(0))
  println(fib(1))
  println(fib(2))
  println(fib(3))
  println(fib(4))
  println(fib(10))





}
