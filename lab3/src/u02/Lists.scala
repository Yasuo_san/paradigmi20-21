package u02

import u02.Optionals.Option.getOrElse
import u03.Lists.List.{Cons, append}
import u03.Lists._
import u02.Optionals._

object Lists extends App {

  def drop[A](l: List[A], n: Int): List[A] = (l,n) match {
    case (Cons(_,t),n) if n>=1 => drop(t,n-1)
    case (Cons(h,t),n) if n==0 => Cons(h,t)
    case (List.Nil(),n) => List.Nil()
  }

  def flatMap[A,B](l: List[A])(f: A => List[B]): List[B] = l match{
    case Cons(h,t) => append(f(h), flatMap(t)(f))
    case _ => List.Nil()
  }

  def map1[A,B](l: List[A])(f: A => B): List[B] = l match{
    case Cons(h,t) => flatMap(l)(e => Cons(f(e), List.Nil()))
    case List.Nil() => List.Nil()
  }

 // def filter1[A,B](l: List[A])(f: A => Boolean): List[B] = flatMap(l) {
 //   case e if f(e) => Cons(e,List.Nil())
  //  case _ => List.Nil()
 // }

  def max(l: List[Int]): Option[Int] = l match{
    case Cons(h,t) if h >= getOrElse(max(t),h) => Optionals.Option.Some(h)
    case Cons(h,t) if h < getOrElse(max(t),h) => max(t)
    case _ => Optionals.Option.None()
  }

  println(max(Cons(10,Cons(25,Cons(20,List.Nil())))))


  val lst = Cons (10 , Cons (20 , Cons (30 , List.Nil() )) )
  //println(filter1(lst)(_>=20)) // Cons (11 , Cons (21 , Cons (31 , Nil ())))

}
