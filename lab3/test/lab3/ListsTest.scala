package lab3

import org.junit.jupiter.api.{Assertions, Test}
import u02.Lists.{drop, flatMap}
import u03.Lists.List.Cons
import u03.Lists._
import u03.Lists.List.Nil

class ListsTest {

  val list: List[Int] = Cons(10, Cons(20, Cons(30, Nil())))

  @Test
  def testDrop(): Unit = {
    Assertions.assertEquals(Cons(20, Cons(30, Nil())), drop(list, 1))
    Assertions.assertEquals(Cons(30, Nil()), drop(list, 2))
    Assertions.assertEquals(Nil(), drop(list, 5))
    Assertions.assertEquals(list, drop(list, 0))
  }

  @Test
  def testFlatMap(): Unit = {
    Assertions.assertEquals(Cons(11, Cons(21, Cons(31, Nil()))), flatMap(list)(v => Cons(v + 1, Nil())))
    Assertions.assertEquals(Cons(11, Cons(12, Cons(21, Cons(22, Cons(31, Cons(32, Nil())))))),
      flatMap(list)(v => Cons(v + 1, Cons(v + 2, Nil()))))
    Assertions.assertEquals(Nil(), flatMap(Nil[Int]())(v => Cons(v + 1, Nil())))
    Assertions.assertEquals(Nil(), flatMap(Nil[Int]())(v => Cons(v + 1, list)))
  }
}
