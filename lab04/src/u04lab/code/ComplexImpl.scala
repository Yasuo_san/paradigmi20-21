package u04lab.code

case class ComplexImpl (override val re: Double,
                   override val im: Double,
                  ) extends Complex {

  override def +(c: Complex): Complex = {
    ComplexImpl(re + c.re, im + c.im)
  }

  override def *(c: Complex): Complex = {
    ComplexImpl(re*c.re - im, im*c.re + re*c.im)
  }
}


