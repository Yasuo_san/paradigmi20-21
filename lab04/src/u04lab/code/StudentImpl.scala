package u04lab.code
import u04lab.code.Lists.List._
import Lists._

case class StudentImpl(override val name:String,
                       override val year: Int) extends Student {

  private var CourseList: List[Course] = Nil()


  override def enrolling(course: Course*): Unit = {
    for(a <- course){
      CourseList = append(Cons(a,Nil()),CourseList)
    }
  }

  override def courses: List[String] = {
    map(CourseList)(c => c.name)
  }

  override def hasTeacher(teacher: String): Boolean = {
    contains(map(CourseList)(t => t.teacher))(teacher)
  }
}
